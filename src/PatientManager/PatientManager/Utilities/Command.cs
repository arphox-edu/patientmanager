﻿using System;
using System.Windows.Input;

namespace PatientManager.Utilities
{
    public sealed class Command : ICommand
    {
        public event EventHandler CanExecuteChanged = delegate { };

        private readonly Action executeMethod;
        private readonly Func<bool> canExecuteMethod;

        public Command(Action executeMethod)
        {
            this.executeMethod = executeMethod;
        }

        public Command(Action executeMethod, Func<bool> canExecuteMethod)
            : this(executeMethod)
        {
            this.canExecuteMethod = canExecuteMethod;
        }

        bool ICommand.CanExecute(object parameter)
        {
            if (canExecuteMethod != null)
                return canExecuteMethod();

            if (executeMethod != null)
                return true;

            return false;
        }

        void ICommand.Execute(object parameter) => executeMethod?.Invoke();

        public void RaiseCanExecuteChanged() => CanExecuteChanged(this, EventArgs.Empty);
    }
}