﻿using PatientManager.Model.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PatientManager.Model
{
    public sealed class PatientFileManager : IPatientFileManager
    {
        private const string FilePath = "patients.data";

        public void SavePatients(IEnumerable<Patient> patients)
        {
            var fileLines = patients.Select(p => p.Name);
            File.WriteAllLines(FilePath, fileLines);
        }

        public IEnumerable<Patient> LoadPatients()
        {
            if (!File.Exists(FilePath))
                return Enumerable.Empty<Patient>();

            return File.ReadAllLines(FilePath)
                .Select(line => new Patient() { Name = line })
                .ToList();
        }
    }
}